FUSE EMULATOR ROMS
==================

These are roms to be used with fuse emulator.
Because some of those roms are hard to find and they disappear from time to time,
I've decided to host a copy.

As you've noticed there's two directories:

- fuse: The entire rom set.

- fuse-extra: The extra roms that aren't part of the fuse emulator, useful if you are a package maintainer.

I've found `this mail <https://gitlab.com/menelkir/fuseroms/-/raw/main/rom-distribution.txt>`_ in `rpm-fusion repository <https://github.com/rpmfusion/fuse-emulator-roms>`_ that shows the position of Amstrad about redistribution of roms.
